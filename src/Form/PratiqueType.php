<?php

namespace App\Form;

use App\Form\SportType;
use App\Entity\Pratique;
use App\Form\PersonneType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PratiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sport', SportType::class, [
            'label' => 'Choisissez un sport'
            ])
            ->add('niveau', ChoiceType::class, [
                'choices' => [
                    'Débutant'=> 'Débutant',
                    'Confirmé'=> 'Confirmé',
                    'Pro'=> 'Pro',
                    'Supporter'=> 'Supporter'
                ]
            ])
            ->add('personne', PersonneType::class, [
                'label' => 'Remplissez vos coordonnées'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pratique::class,
        ]);
    }
}
